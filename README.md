# README #

## About ##

**winsay-cli v0.5 alpha**

Text-To-Speech with [System.Speech.Synthesis](https://msdn.microsoft.com/de-de/library/System.Speech.Synthesis.aspx) and [NDesk.Options](http://www.ndesk.org/Options). Inspired by OS X's say, but not nearly as sophisticated as it. ;)

I'm not very experienced with C# and wrote this small application primarily for learning (and fun ;)) purpose. Improvements are welcome!

## Usage ##

1. Extract [winsay-cli-0.5.zip](https://bitbucket.org/xae1/winsay-cli/downloads/winsay-cli-0.5.zip) contents to a desired location
2. Press `WINKEY` + `R`, input `cmd` and press `ENTER` to open the commandline prompt
3. Navigate to the folder which contains the executable files: `cd path/to/winsay-cli`
4. Run `winsay hello world` for speaking "hello world" with default option settings
5. Run `winsay --help` for more information about available options

Option  | Description
------------- | -------------
`-v name`, `--voice=name`  | The name of the **voice**. If the name contains spaces, it has to be quoted. Use ? as name to show the installed voices
`-r value`, `--rate=value`  | Speech **rate** value. This must be an integer between -10 and 10
`-d`, `--debug` | Increase **debug** message verbosity
`-h`, `--help` | Show **help** and exit
`-t value`, `--time=value` | Speak in **time** seconds (delay)

For easier use, you can add winsay to the Windows PATH variable. This way, you can use the `winsay` command everywhere.

- [http://www.computerhope.com/issues/ch000549.htm](http://www.computerhope.com/issues/ch000549.htm)
- [https://java.com/en/download/help/path.xml](https://java.com/en/download/help/path.xml)

## Contribution guidelines ##

None. Want to contribute? [Contact](https://bitbucket.org/xae1)

## Who do I talk to? ##

* Xael
	* [https://bitbucket.org/xae1](https://bitbucket.org/xae1)