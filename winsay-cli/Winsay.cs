﻿using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;

/// <summary>
/// Provides access to the built-in Text-To-Speech functionality (System.Speech.Synthesis).
/// </summary>
public class Winsay
{
    private SpeechSynthesizer synth;
    private PromptBuilder builder;
    private IReadOnlyCollection<InstalledVoice> voices;

    public Winsay()
    {
        synth = new SpeechSynthesizer();
        builder = new PromptBuilder();
        voices = synth.GetInstalledVoices();
        synth.Rate = 0;
        synth.SetOutputToDefaultAudioDevice();
        synth.Volume = 100;
    }

    /// <summary>
    /// Sets the speech rate.
    /// </summary>
    /// <param name="rate">Integer between -10 and 10</param>
    public void SetRate(int rate)
    {
        if (rate >= -10 && rate <= 10)
        {
            synth.Rate = rate;
        }
    }

    /// <summary>
    /// Returns the current speech rate.
    /// </summary>
    /// <returns>Speech rate</returns>
    public int GetRate()
    {
        return synth.Rate;
    }

    /// <summary>
    /// Sets the voice by name.
    /// </summary>
    /// <param name="name">Name of the voice</param>
    public void SetVoice(string name)
    {
        synth.SelectVoice(name);
    }

    /// <summary>
    /// Sets the voice by index.
    /// </summary>
    /// <param name="index">Index of the voice</param>
    public void SetVoice(int index)
    {
        synth.SelectVoice(voices.ElementAt(index).VoiceInfo.Name);
    }

    /// <summary>
    /// Return the name of the current voice.
    /// </summary>
    /// <returns>Name of current voice</returns>
    public string GetVoice()
    {
        return synth.Voice.Name;
    }

    /// <summary>
    /// Get installed (available) voices
    /// </summary>
    public IReadOnlyCollection<InstalledVoice> GetInstalledVoices()
    {
        return voices;
    }

    // Todo
    public void SetOutputTo(string type, string path = "")
    {
        switch (type)
        {
            case "device":
                synth.SetOutputToDefaultAudioDevice();
                break;
            case "file":
                synth.SetOutputToWaveFile(path);
                break;
        }
    }

    /// <summary>
    /// Appends text to the prompt builder.
    /// </summary>
    /// <param name="text">Text to append</param>
    public void AppendText(string text)
    {
        builder.AppendText(text);
    }

    /// <summary>
    /// Speaks the prompt builder content.
    /// </summary>
    public void Say()
    {
        synth.Speak(builder);
    }

    /// <summary>
    /// Speaks a simple string.
    /// </summary>
    /// <param name="text">Text to speak</param>
    public void Say(string text)
    {
        synth.Speak(text);
    }

    /// <summary>
    /// Speaks a string array.
    /// </summary>
    /// <param name="text">Text to speak</param>
    public void Say(string[] text)
    {
        synth.Speak(string.Join(" ", text));
    }

    /// <summary>
    /// Disposes the synthesizer object
    /// </summary>
    public void Dispose()
    {
        synth.Dispose();
    }
}
