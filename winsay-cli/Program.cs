﻿using System;
using System.Collections.Generic;
using System.Linq;
using NDesk.Options;
using System.Diagnostics;

namespace winsay
{
    class Program
    {
        static int verbosity;

        static void Main(string[] args)
        {
            Winsay winsay = new Winsay();

            bool showHelp = false;

            string voice = "";
            int time = 0;
            int rate = 0;
            List<string> text = new List<string>();

            // Option settings
            var p = new OptionSet() {
                { "v|voice=", "The {index} of the voice.\nUse ? as name to show the installed voices.",
                    value => voice = value },
                { "r|rate=", "Speech rate {value}.\n This must be an integer between -10 and 10.",
                    (int value) => rate = value },
                { "d|debug", "Increase debug message verbosity",
                    value => { if (value != null) ++verbosity; } },
                { "h|help",  "Show help and exit",
                    value => showHelp = value != null },
                { "t|time=", "Speak in {time} seconds (delay)",
                    (int value) => time = value }
            };

            // Parse option arguments
            try
            {
                text = p.Parse(args); // Returns remaining arguments = text to speak
            }
            catch (OptionException e)
            {
                Console.Write("Error: ");
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `winsay --help' for more information.");
                winsay.Dispose();
                return;
            }

            // Show help
            if (showHelp)
            {
                ShowHelp(p);
                winsay.Dispose();
                return;
            }

            // Show voices
            if (voice == "?")
            {
                ShowInstalledVoices(winsay);
                winsay.Dispose();
                return;
            }

            // Speak
            else
            {
                // Set rate
                if (rate != 0)
                {
                    winsay.SetRate(rate);
                    Debug("Rate set to {0}", rate);
                }
                else
                {
                    Debug("Rate set to default (" + winsay.GetRate() + ")");
                }

                // Set specific voice
                if (voice != "")
                {
                    int result;
                    Int32.TryParse(voice, out result);
                    winsay.SetVoice(result);
                    Debug("Voice set to {0}", result);
                }
                else
                {
                    Debug("Voice set to default ({0})", winsay.GetVoice());
                }

                // Text
                if (text.Count > 0)
                {
                    // Timer
                    if (time > 0)
                    {
                        Debug("Delay set to {0} seconds", time);

                        Stopwatch sw = new Stopwatch();

                        sw.Start();
                        while (true)
                        {
                            if (sw.ElapsedMilliseconds > (time * 1000))
                            {
                                winsay.Say(text.ToArray());
                                return;
                            }
                        }
                        sw.Stop();
                    }
                    else
                    {
                        Debug("Delay set to default ({0} seconds)", time);
                        winsay.Say(text.ToArray());
                    }

                    Debug("Text set to \"{0}\"", string.Join(" ", text));
                    winsay.Dispose();
                    return;
                }
                else {
                    ShowHelp(p);
                    Debug("No text specified. Show help.");
                    winsay.Dispose();
                    return;
                }
            }
        }

        /// <summary>
        /// Shows the help dialog
        /// </summary>
        /// <param name="p"></param>
        static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("winsay-cli v0.5\n");
            Console.WriteLine("Text-To-Speech with System.Speech.Synthesis and NDesk.Options.Inspired by OS X's say, but not nearly as sophisticated as it. ;)\n");
            Console.WriteLine("Usage: winsay [OPTIONS]+ text \n");
            Console.WriteLine("If no options are specified, default settings are used.\n");
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }

        /// <summary>
        /// Shows installed voices and their details
        /// </summary>
        /// <param name="winsay"></param>
        static void ShowInstalledVoices(Winsay winsay)
        {
            Console.WriteLine("Available (installed) voices:\n");

            int i = 0;
            foreach (var voice in winsay.GetInstalledVoices().Select(voice => voice.VoiceInfo))
            {
                Console.WriteLine("{4}. {0} ({1}) {2}, {3}", voice.Name, voice.Culture, voice.Gender, voice.Age, i++);
            }
        }

        /// <summary>
        /// Prints debug messages if verbosity is > 0
        /// </summary>
        /// <param name="format">Format string</param>
        /// <param name="args">Arguments to debug</param>
        static void Debug(string format, params object[] args)
        {
            if (verbosity > 0)
            {
                Console.Write("# ");
                Console.WriteLine(format, args);
            }
        }

    }
}